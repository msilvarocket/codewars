package com.challenge.codewar.memberscreen

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.challenge.codewar.CodeWarsApplication
import com.challenge.codewar.R
import com.challenge.codewar.challengedetailsscreen.ChallengeDetailsFragment
import com.challenge.codewar.database.pojo.*
import com.challenge.codewar.mainscreen.MainActivity
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.fragment_member_details.*
import javax.inject.Inject

/**
 * MemberDetailsFragment screen contains Member Details,
 * a list of Completed Challenges and a list of Authored Challenges
 */
class MemberDetailsFragment : Fragment(), MemberDetailsPresenter.Callback, ChallengesAdapter.Callback,
    AuthoredAdapter.Callback {

    @Inject
    lateinit var presenter: MemberDetailsPresenter

    private val completedChallengesAdapter: ChallengesAdapter = ChallengesAdapter(mutableListOf(), this)
    private val authoredAdapter: AuthoredAdapter = AuthoredAdapter(mutableListOf(), this)
    private var selectedPosition = 0
    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_completed -> {
                recyclerViewCompletedContainer.visibility = VISIBLE
                recyclerViewAuthoredContainer.visibility = GONE
                selectedPosition = 0
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_authored -> {
                recyclerViewAuthoredContainer.visibility = VISIBLE
                recyclerViewCompletedContainer.visibility = GONE
                selectedPosition = 1
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        CodeWarsApplication.applicationComponent.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_member_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.let {
            recyclerViewCompleted?.apply {
                addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
                adapter = completedChallengesAdapter
                val linearLayoutManager = LinearLayoutManager(context)
                this.layoutManager = linearLayoutManager
                visibility = VISIBLE
            }
            recyclerViewAuthored?.apply {
                addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
                adapter = authoredAdapter
                val linearLayoutManager = LinearLayoutManager(context)
                this.layoutManager = linearLayoutManager
                visibility = VISIBLE
            }

            bottom_navigation_view.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
            it.getString(KEY_USERNAME)?.let { username ->
                activity?.apply {
                    title = username
                    when (this) {
                        is MainActivity -> showBackButton(true)
                    }
                }

                presenter.attach(this, username)
            }

        }
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detach()
    }

    /**
     * Callback from Presenter
     */
    override fun onViewRestored(userName: String) {
        member_info.visibility = VISIBLE
        username.text = userName
        if (completedChallengesAdapter.itemCount == 0) {
            onCompletedChallengesEmpty()
        }

        if (authoredAdapter.itemCount == 0) {
            onAuthoredEmpty()
        }

        recyclerViewCompletedContainer.visibility = if (selectedPosition == 0) VISIBLE else GONE
        recyclerViewAuthoredContainer.visibility = if (selectedPosition == 1) VISIBLE else GONE
    }

    override fun onMemberDetails(memberComplete: MemberComplete) {
        activity?.let {
            member_info.visibility = VISIBLE
            username.text = memberComplete.username
        }
    }

    override fun onCompletedChallenges(completedChallenges: CompletedChallenges) {
        completedChallenges.completedData?.apply {
            completedChallengesAdapter.addItems(this)
        }
        completedEmpty.visibility = GONE
    }

    override fun onAuthored(authoredChallenges: AuthoredChallenges) {
        authoredChallenges.authoredData?.apply {
            if (this.isNotEmpty()) {
                authoredAdapter.addItems(this)
            } else {
                onAuthoredEmpty()
            }
        }
    }

    override fun onAuthoredEmpty() {
        recyclerViewAuthored.visibility = GONE
        authoredEmpty.visibility = VISIBLE
    }

    override fun onCompletedChallengesEmpty() {
        recyclerViewCompleted.visibility = GONE
        completedEmpty.visibility = VISIBLE
    }

    override fun onError(message: String) {
        activity?.apply {
            Toast.makeText(this, message, Toast.LENGTH_LONG).show()
        }
    }

    override fun onLoading(boolean: Boolean) {
        loading.visibility = if (boolean) VISIBLE else GONE
    }

    /**
     * Callback from Adapters
     * #onItemClicked - ChallengesAdapter
     */
    override fun onItemClicked(completedData: CompletedData) {
        activity?.apply {
            Navigation.findNavController(this, R.id.nav_fragment)
                .navigate(R.id.challenge_details_fragment, Bundle().apply {
                    putInt(ChallengeDetailsFragment.KEY_CHALLENGE_TYPE, ChallengeDetailsFragment.TYPE_COMPLETED)
                    putString(ChallengeDetailsFragment.KEY_CHALLENGE_ID, completedData.id)
                })
        }
    }

    /**
     * AuthoredAdapter item click callback
     */
    override fun onAuthoredClicked(authoredData: AuthoredData) {
        activity?.apply {
            Navigation.findNavController(this, R.id.nav_fragment)
                .navigate(R.id.challenge_details_fragment, Bundle().apply {
                    putInt(ChallengeDetailsFragment.KEY_CHALLENGE_TYPE, ChallengeDetailsFragment.TYPE_AUTHORED)
                    putString(ChallengeDetailsFragment.KEY_CHALLENGE_ID, authoredData.id)
                })
        }
    }

    /**
     * Last Item binded, get more if needed.
     */
    override fun onLastItemBinded() {
        presenter.getNextPageCompleted()
    }

    override fun onNoMoreItems() {
        activity?.apply {
            Toast.makeText(this, "No more items to display", Toast.LENGTH_LONG).show()
        }
    }

    companion object {
        const val KEY_USERNAME = "username"
    }
}