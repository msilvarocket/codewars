package com.challenge.codewar.memberscreen

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.challenge.codewar.R
import com.challenge.codewar.database.pojo.AuthoredData
import com.challenge.codewar.extensions.setOnThrottleClickListener

class AuthoredAdapter(private val mutableList: MutableList<AuthoredData>,
                      private val callback: Callback) : RecyclerView.Adapter<AuthoredAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).
                inflate(
                    R.layout.adapter_authored_challenges_element, parent,
                    false))
    }

    override fun getItemCount(): Int {
        return mutableList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        mutableList[position].let { authored ->
            authored.apply {
                holder.name.text = name
                holder.rank.text = rankName
                holder.itemView.setOnThrottleClickListener {
                    callback.onAuthoredClicked(authored)
                }
            }
        }
    }

    fun addItems(list: List<AuthoredData>){
        mutableList.addAll(list)
        notifyDataSetChanged()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val name : TextView = itemView.findViewById(R.id.name)
        val rank : TextView = itemView.findViewById(R.id.rank)
    }

    interface Callback {
        fun onAuthoredClicked(authoredData: AuthoredData)
    }
}