package com.challenge.codewar.memberscreen

import com.challenge.codewar.database.pojo.AuthoredChallenges
import com.challenge.codewar.database.pojo.CompletedChallenges
import com.challenge.codewar.database.pojo.MemberComplete
import com.challenge.codewar.searchscreen.SearchService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class MemberDetailsPresenter @Inject constructor(private val searchService: SearchService) {

    private val disposables by lazy { CompositeDisposable() }
    private var pageCompletedChallenges: Int = 0
    private var pagesCompleted: Int = 0
    private var userName: String = ""
    interface Callback {
        fun onMemberDetails(memberComplete: MemberComplete)
        fun onCompletedChallenges(completedChallenges: CompletedChallenges)
        fun onAuthored(authoredChallenges: AuthoredChallenges)
        fun onCompletedChallengesEmpty()
        fun onAuthoredEmpty()
        fun onNoMoreItems()
        fun onError(message: String)
        fun onLoading(boolean: Boolean)
        fun onViewRestored(userName: String)
    }

    var callback: Callback? = null

    fun attach(callback: Callback, userName: String) {
        this.callback?.let {
            it.onViewRestored(userName)
        }?: run {
            this.callback = callback
            this.userName = userName
            getMemberDetails(userName)
        }
    }

    fun detach() {
        this.callback = null
        disposables.clear()
    }

    /**
     * Get Member Complete Info from DB
     * Fetch Member Completed Challenges page 0
     * Fetch Member Authored Challenges
     *
     */
    private fun getMemberDetails(userName: String) {
        searchService.getMemberComplete(userName)
            .subscribeOn(Schedulers.io())
            .doOnSubscribe { callback?.onLoading(true) }
            .flatMap {
                searchService.getChallengesCompleted(userName, pageCompletedChallenges).map {
                        r-> Pair(it,r)  }
            }
            .flatMap {
                searchService.getAuthored(userName).map { t-> Triple(it.first, it.second, t) }
            }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                callback?.onMemberDetails(it.first)
                callback?.onLoading(false)
                if(it.second.totalItems > 0){
                    pageCompletedChallenges += 1
                    pagesCompleted = it.second.totalPages
                    callback?.onCompletedChallenges(it.second)
                } else {
                    callback?.onCompletedChallengesEmpty()
                }

                it.third.authoredData?.let { _->
                    callback?.onAuthored(it.third)
                }?: run {
                    callback?.onAuthoredEmpty()
                }

            }, {
                callback?.onLoading(false)
                it.message?.apply {
                    callback?.onError(this)
                }
            }).also {
                disposables.add(it)
            }
    }

    /**
     * Fetch next page from Completed Challenges for the current member
     * before fetching checks whether we reached the final page.
     */
    fun getNextPageCompleted(){
        if(pageCompletedChallenges < pagesCompleted) {
            searchService.getNextChallengesCompleted(userName, pageCompletedChallenges)
                .subscribeOn(Schedulers.io())
                .doOnSubscribe { callback?.onLoading(true) }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    callback?.onLoading(false)
                    if(it.totalItems > 0){
                        pageCompletedChallenges += 1
                        callback?.onCompletedChallenges(it)
                    } else {
                        callback?.onCompletedChallengesEmpty()
                    }
                }, {
                    callback?.onLoading(false)
                    it.message?.apply {
                        callback?.onError(this)
                    }
                }).also {
                    disposables.add(it)
                }
        } else {
            callback?.onNoMoreItems()
        }
    }
}