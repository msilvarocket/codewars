package com.challenge.codewar.memberscreen

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.challenge.codewar.R
import com.challenge.codewar.database.pojo.CompletedData
import com.challenge.codewar.extensions.setOnThrottleClickListener

class ChallengesAdapter(private val mutableList: MutableList<CompletedData>,
                        private val callback: Callback) : RecyclerView.Adapter<ChallengesAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).
                inflate(
                    R.layout.adapter_completed_challenges_element, parent,
                    false))
    }

    override fun getItemCount(): Int {
        return mutableList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        mutableList[position].let { challenges ->
            if(position == mutableList.size - 1){
                callback.onLastItemBinded()
            }
            challenges.apply {
                holder.name.text = name
                holder.completedAt.text = completedAt
                holder.itemView.setOnThrottleClickListener {
                    callback.onItemClicked(challenges)
                }
            }
        }
    }

    fun addItems(list: List<CompletedData>){
        mutableList.addAll(list)
        notifyDataSetChanged()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val name : TextView = itemView.findViewById(R.id.name)
        val completedAt : TextView = itemView.findViewById(R.id.completed_at)
    }

    interface Callback {
        fun onItemClicked(completedData: CompletedData)
        fun onLastItemBinded()
    }
}