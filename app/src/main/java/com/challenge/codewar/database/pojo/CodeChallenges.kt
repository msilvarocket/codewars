package com.challenge.codewar.database.pojo

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity
data class CodeChallenges(
    @ForeignKey(entity = Member::class, parentColumns = ["username"], childColumns = ["username"], onDelete = ForeignKey.CASCADE)
    val username: String,
    val totalAuthored: String?,
    val totalCompleted: String?
){
    @PrimaryKey(autoGenerate = true)
    var idCC: Int = 0
}
