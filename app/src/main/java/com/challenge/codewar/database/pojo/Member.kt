package com.challenge.codewar.database.pojo

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.challenge.codewar.database.Converters
import java.util.*

@Entity(tableName = "member_table")
@TypeConverters(Converters::class)
open class Member (@PrimaryKey
                   val username: String){
    var name: String? = null
    var honor: Int? = null
    var clan: String? = null
    var leaderboardPosition: Int = 0
    var date: Date = Date()
}

