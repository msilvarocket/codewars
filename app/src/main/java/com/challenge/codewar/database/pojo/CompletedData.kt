package com.challenge.codewar.database.pojo

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.challenge.codewar.database.Converters

@Entity
@TypeConverters(Converters::class)
data class CompletedData(
    @ForeignKey(entity = Member::class, parentColumns = ["username"], childColumns = ["username"], onDelete = ForeignKey.CASCADE)
    val username: String,
    @PrimaryKey
    var id: String,
    var name: String,
    var slug: String,
    var completedAt: String,
    var completedLanguages:  ArrayList<String>?
)



