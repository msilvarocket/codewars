package com.challenge.codewar.database.pojo

import androidx.room.Relation

class MemberComplete(username: String) : Member(username) {
    @Relation(parentColumn = "username", entityColumn = "username", entity = Skill::class)
    var skills: List<Skill>? = null

    @Relation(parentColumn = "username", entityColumn = "username", entity = Overall::class)
    var overall: Set<Overall>? = null

    @Relation(parentColumn = "username", entityColumn = "username", entity = Languages::class)
    var languages: List<Languages>? = null

    @Relation(parentColumn = "username", entityColumn = "username", entity = CodeChallenges::class)
    var codeChallenges: Set<CodeChallenges>? = null
}