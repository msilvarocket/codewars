package com.challenge.codewar.database.pojo

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(tableName = "completed_info")
open class CompletedInfo(
    @ForeignKey(entity = Member::class, parentColumns = ["username"], childColumns = ["username"], onDelete = ForeignKey.CASCADE)
    val username: String,
    var totalPages: Int,
    var totalItems: Int = 0
) {
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0
}



