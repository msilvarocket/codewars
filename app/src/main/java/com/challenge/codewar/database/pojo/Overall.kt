package com.challenge.codewar.database.pojo

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
open class Overall (
    @ForeignKey(entity = Member::class, parentColumns = ["username"], childColumns = ["username"], onDelete = ForeignKey.CASCADE)
    var username: String,
    val rank: Int?,
    val overallName: String?,
    val color: String?,
    val score: Int?
    ) {
    @PrimaryKey(autoGenerate = true)
    var idOverall: Int = 0
}