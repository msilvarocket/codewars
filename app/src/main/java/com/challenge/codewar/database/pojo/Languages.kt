package com.challenge.codewar.database.pojo

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.ForeignKey.CASCADE
import androidx.room.PrimaryKey

@Entity
data class Languages(
    @ForeignKey(entity = Languages::class, parentColumns = ["username"], childColumns = ["username"], onDelete = CASCADE)
    val username: String,
    val language: String,
    val rank: Int?,
    val languageName: String?,
    val color: String?,
    val score: Int?
){
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0
}
