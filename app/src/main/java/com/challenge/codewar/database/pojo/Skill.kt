package com.challenge.codewar.database.pojo

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
open class Skill(
    val text: String? = null,
    @ForeignKey(
        entity = Skill::class,
        parentColumns = ["username"],
        childColumns = ["username"],
        onDelete = ForeignKey.CASCADE
    )
    val username: String
) {
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0
}
