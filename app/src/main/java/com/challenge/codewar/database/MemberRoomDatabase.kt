package com.challenge.codewar.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.challenge.codewar.database.pojo.*

@Database(
    entities = [Member::class, Languages::class, Overall::class, CodeChallenges::class, Skill::class,
        CompletedInfo::class, CompletedData::class, AuthoredInfo::class, AuthoredData::class],
    version = 1,
    exportSchema = false
)
@TypeConverters(Converters::class)
abstract class MemberRoomDatabase : RoomDatabase() {
    abstract fun memberDao(): MemberDao
}