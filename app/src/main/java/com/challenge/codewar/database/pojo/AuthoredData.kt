package com.challenge.codewar.database.pojo

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.challenge.codewar.database.Converters

@Entity
@TypeConverters(Converters::class)
data class AuthoredData(
    @ForeignKey(entity = Member::class, parentColumns = ["username"], childColumns = ["username"], onDelete = ForeignKey.CASCADE)
    val username: String,
    @PrimaryKey
    var id: String,
    var name: String?,
    var description: String?,
    var rank: Int?,
    var rankName: String?,
    var tags:  ArrayList<String>?,
    var languages:  ArrayList<String>?
)