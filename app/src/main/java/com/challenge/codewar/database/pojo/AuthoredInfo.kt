package com.challenge.codewar.database.pojo

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(tableName = "authored_info")
open class AuthoredInfo(
    @ForeignKey(entity = Member::class, parentColumns = ["username"], childColumns = ["username"], onDelete = ForeignKey.CASCADE)
    val username: String
) {
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0
}



