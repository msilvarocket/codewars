package com.challenge.codewar.database.pojo

import androidx.room.Relation

class AuthoredChallenges(
    username: String
) : AuthoredInfo(username){

    @Relation(parentColumn = "username", entityColumn = "username", entity = AuthoredData::class)
    var authoredData: List<AuthoredData>? = null
}
