package com.challenge.codewar.database

import android.util.Log
import androidx.room.*
import com.challenge.codewar.database.pojo.*
import com.challenge.codewar.network.pojo.AuthoredChallengesResponse
import com.challenge.codewar.network.pojo.CompletedChallengesResponse
import com.challenge.codewar.network.pojo.MemberResponse


@Dao
abstract class MemberDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insert(member: Member)

    @Transaction
    open fun inserMemberResponse(memberResponse: MemberResponse) : MemberComplete {
        memberResponse.apply {
            Member(username).also {
                it.name = name
                it.honor = honor
                it.clan = clan
                it.leaderboardPosition = leaderboardPosition
                insert(it)
            }
            ranks?.languages?.forEach { language ->
                insert(
                    Languages(
                        username,
                        language.key,
                        language.value.rank,
                        language.value.name,
                        language.value.color,
                        language.value.score
                    )
                )
            }
            ranks?.overall?.apply {
                insert(Overall(username, rank, name, color, score))
            }
            codeChallenges?.apply {
                insert(CodeChallenges(username, totalAuthored, totalCompleted))
            }

            skills?.forEach { insert(Skill(it, username)) }
        }
        return getMemberComplete(memberResponse.username)
    }

    @Transaction
    open fun inserCompletedChallenges(username: String, completedChallengesResponse: CompletedChallengesResponse) : CompletedChallenges {
        completedChallengesResponse.apply {
            insert(CompletedInfo(username, totalPages, totalItems))
            data?.forEach {
                insert(CompletedData(username,it.id, it.name, it.slug, it.completedAt, it.completedLanguages))
            }

        }
        return getCompletedChallenges(username)
    }

    @Transaction
    open fun inserAuthoredChallenges(username: String, authoredChallengesResponse: AuthoredChallengesResponse) : AuthoredChallenges {
        authoredChallengesResponse.apply {
            insert(AuthoredInfo(username))
            data?.forEach {
                insert(AuthoredData(username,it.id, it.name, it.description, it.rank, it.rankName, it.tags, it.languages))
            }

        }
        return getAuthoredChallenges(username)
    }

    @Transaction
    @Query("SELECT * from member_table WHERE username = :username")
    abstract fun getMemberComplete(username: String): MemberComplete

    @Transaction
    @Query("SELECT * from completed_info WHERE username = :username")
    abstract fun getCompletedChallenges(username: String): CompletedChallenges

    @Transaction
    @Query("SELECT * from authored_info WHERE username = :username")
    abstract fun getAuthoredChallenges(username: String): AuthoredChallenges

    @Transaction
    @Query("SELECT * from completeddata WHERE id = :id")
    abstract fun getCompletedData(id: String): CompletedData

    @Transaction
    @Query("SELECT * from authoreddata WHERE id = :id")
    abstract fun getAuthoredData(id: String): AuthoredData

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insert(languages: Languages)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insert(overall: Overall)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insert(codeChallenges: CodeChallenges)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insert(skill: Skill)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insert(completedInfo: CompletedInfo)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insert(authoredInfo: AuthoredInfo)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insert(completedData: CompletedData)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insert(authoredData: AuthoredData)

    @Transaction
    @Query("SELECT DISTINCT * from member_table ORDER BY date DESC")
    abstract fun getCachedMembers(): MutableList<MemberComplete>

    @Query("SELECT DISTINCT COUNT(*) from member_table")
    abstract fun getNumberOfMembers(): Int

    @Query("SELECT COUNT(*) from member_table WHERE username = :username")
    abstract fun containsMember(username: String): Int

    @Query("SELECT * from member_table ORDER BY date ASC LIMIT 1")
    abstract fun getMemberToDelete(): Member

    @Query("DELETE FROM member_table WHERE username = :username")
    abstract fun delete(username: String)

    @Delete
    abstract fun delete(member: Member)

    @Query("DELETE FROM member_table")
    abstract fun deleteAll()
}