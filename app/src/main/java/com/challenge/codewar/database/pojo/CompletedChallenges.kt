package com.challenge.codewar.database.pojo

import androidx.room.Relation

class CompletedChallenges(
    username: String,
    totalPages: Int,
    totalItems: Int = 0
) : CompletedInfo(username, totalPages, totalItems){

    @Relation(parentColumn = "username", entityColumn = "username", entity = CompletedData::class)
    var completedData: List<CompletedData>? = null
}
