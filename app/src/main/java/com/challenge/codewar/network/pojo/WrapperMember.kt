package com.challenge.codewar.network.pojo

import com.challenge.codewar.database.pojo.MemberComplete


sealed class WrapperMember constructor(val status: Int) {
    class Error(status: Int, val errorMessage: String) : WrapperMember(status)
    class Add(status: Int, val memberComplete: MemberComplete) : WrapperMember(status)
    class Delete(status: Int, val username: String) : WrapperMember(status)
    companion object {
        const val STATUS_FOUND = 0
        const val STATUS_REMOVED = 1
        const val STATUS_TO_REMOVE = 2
        const val STATUS_FAILED = 3
    }
}