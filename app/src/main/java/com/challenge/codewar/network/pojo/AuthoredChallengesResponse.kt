package com.challenge.codewar.network.pojo

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class AuthoredChallengesResponse {
    var data: ArrayList<AuthoredDataResponse>? = null
}