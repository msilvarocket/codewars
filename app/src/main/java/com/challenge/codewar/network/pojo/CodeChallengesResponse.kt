package com.challenge.codewar.network.pojo

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
open class CodeChallengesResponse(
    var totalAuthored: String? = null,
    var totalCompleted: String? = null
)