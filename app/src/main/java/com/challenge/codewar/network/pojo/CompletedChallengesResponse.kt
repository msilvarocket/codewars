package com.challenge.codewar.network.pojo

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class CompletedChallengesResponse {
    var totalPages: Int = 0
    var totalItems: Int = 0
    var data: ArrayList<CompletedDataResponse>? = null
}