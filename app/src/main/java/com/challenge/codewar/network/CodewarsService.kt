package com.challenge.codewar.network

import com.challenge.codewar.network.pojo.AuthoredChallengesResponse
import com.challenge.codewar.network.pojo.CompletedChallengesResponse
import com.challenge.codewar.network.pojo.MemberResponse
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface CodewarsService {
    @GET("users/{user_name}")
    fun searchUser(@Path("user_name") user_name : String) : Single<Response<MemberResponse>>

    @GET("users/{user_name}/code-challenges/completed")
    fun getChallengesCompleted(@Path("user_name") user_name : String, @Query("page") page: Int) : Single<Response<CompletedChallengesResponse>>

    @GET("users/{user_name}/code-challenges/authored")
    fun getAuthored(@Path("user_name") user_name : String) : Single<Response<AuthoredChallengesResponse>>
}