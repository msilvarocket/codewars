package com.challenge.codewar.network.pojo

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class MemberResponse {
    var username: String = ""
    var name: String? = ""
    var honor: Int? = -1
    var clan: String? = ""
    var leaderboardPosition: Int = -1
    var skills: ArrayList<String>? = null
    var ranks: RanksResponse? = null
    var codeChallenges: CodeChallengesResponse?= null
}
