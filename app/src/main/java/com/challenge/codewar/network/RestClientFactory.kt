package com.challenge.codewar.network

import okhttp3.OkHttpClient
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.jackson.JacksonConverterFactory

object RestClientFactory {

    /**
     * Creates a retrofit service from an arbitrary class (clazz)
     * @param endPoint REST endpoint url
     * @return retrofit service with defined endpoint
     */
    fun createRetrofitRestClient(
        endPoint: String,
        okHttpClient: OkHttpClient,
        factory: Converter.Factory = JacksonConverterFactory.create()
    ): CodewarsService {
        val retrofit = Retrofit.Builder()
            .client(okHttpClient)
            .baseUrl(endPoint)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(factory)
            .build()

        return retrofit.create(CodewarsService::class.java)
    }


}