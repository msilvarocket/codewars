package com.challenge.codewar.network.pojo

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty



@JsonIgnoreProperties(ignoreUnknown = true)
open class LanguagesResponse(
    var language : String? = null, var overallResponse: OverallResponse? = null)
