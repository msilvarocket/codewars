package com.challenge.codewar.network.pojo

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty



@JsonIgnoreProperties(ignoreUnknown = true)
open class RanksResponse(
    var overall: OverallResponse? = null,
    var languages: Map<String, OverallResponse>? = null
) {
    @JsonProperty("languages")
    private fun unpackNested(languages: Map<String, OverallResponse>) {
        this.languages = languages
    }
}