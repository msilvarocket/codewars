package com.challenge.codewar.network.pojo

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class CompletedDataResponse {
    var id: String = ""
    var name: String = ""
    var slug: String = ""
    var completedAt: String = ""
    var completedLanguages: ArrayList<String>? = null

}