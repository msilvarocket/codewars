package com.challenge.codewar.network.pojo

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class AuthoredDataResponse {
    var id: String = ""
    var name: String? = ""
    var description: String? = ""
    var rank: Int? = 0
    var rankName: String? = ""
    var tags:  ArrayList<String>? = null
    var languages:  ArrayList<String>? = null
}