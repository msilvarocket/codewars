package com.challenge.codewar.network.pojo

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
open class OverallResponse (
    val rank: Int? = null,
    val name: String? = null,
    val color: String? = null,
    val score: Int? = null
    )