package com.challenge.codewar.extensions

import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager

/**
 * Register a callback to be invoked when this view is clicked. If this view is not clickable, it becomes clickable.
 *
 * @see .setClickable
 */
fun View.setOnThrottleClickListener(block: (View) -> Unit) {
    var ts = 0L
    this.setOnClickListener { v ->
        System.currentTimeMillis().let { t ->
            if ((t - ts) > 200) {
                block(v).also { ts = t }
            }
        }
    }
}

/**
 * Hides the keyboard
 *
 * @see [InputMethodManager]
 */
fun View.hideKeyboard() {
    val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(windowToken, 0)
}