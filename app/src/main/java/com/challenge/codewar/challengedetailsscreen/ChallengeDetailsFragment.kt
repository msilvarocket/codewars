package com.challenge.codewar.challengedetailsscreen

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.challenge.codewar.CodeWarsApplication
import com.challenge.codewar.R
import com.challenge.codewar.database.pojo.AuthoredData
import com.challenge.codewar.database.pojo.CompletedData
import kotlinx.android.synthetic.main.fragment_challenge_details.*
import kotlinx.android.synthetic.main.fragment_member_details.loading
import javax.inject.Inject

class ChallengeDetailsFragment : Fragment(), ChallengeDetailsPresenter.Callback {

    @Inject
    lateinit var presenter: ChallengeDetailsPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        CodeWarsApplication.applicationComponent.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_challenge_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.let {
            val type = it.getInt(KEY_CHALLENGE_TYPE, 0)
            it.getString(KEY_CHALLENGE_ID)?.let { challenge ->
                activity?.title = challenge
                presenter.attach(this, type, challenge)
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.detach()
    }

    override fun onError(message: String) {
        activity?.apply {
            Toast.makeText(this, message, Toast.LENGTH_LONG).show()
        }
    }

    override fun onLoading(boolean: Boolean) {
        loading.visibility = if (boolean) VISIBLE else GONE
    }

    override fun onAuthoredData(authoredChallenge: AuthoredData) {
        authored_challenge.visibility = VISIBLE
        name_authored.text = authoredChallenge.name
        description.text = authoredChallenge.description
        rank_authored.text = authoredChallenge.rankName.plus(" ").plus(authoredChallenge.rank)

        var tagsText = ""
        authoredChallenge.tags?.forEach { t -> tagsText = tagsText.plus(t).plus(" ") }
        tags_authored.text = tagsText

        var languageText = ""
        authoredChallenge.languages?.forEach { l -> languageText = languageText.plus(l).plus(" ") }
        languages_authored.text = languageText
    }

    override fun onCompletedData(completedChallenge: CompletedData) {
        completed_challenge.visibility = VISIBLE
        name.text = completedChallenge.name
        slug.text = completedChallenge.slug
        completedAt.text = completedChallenge.completedAt

        var languageText = ""
        completedChallenge.completedLanguages?.forEach { l -> languageText = languageText.plus(l).plus(" ") }
        languages_value.text = languageText
    }

    companion object {
        const val KEY_CHALLENGE_TYPE = "type"
        const val KEY_CHALLENGE_ID = "challenge_id"
        const val TYPE_COMPLETED = 0
        const val TYPE_AUTHORED = 1
    }
}