package com.challenge.codewar.challengedetailsscreen

import com.challenge.codewar.database.pojo.AuthoredData
import com.challenge.codewar.database.pojo.CompletedData
import com.challenge.codewar.searchscreen.SearchService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class ChallengeDetailsPresenter @Inject constructor(private val searchService: SearchService) {

    private val disposables by lazy { CompositeDisposable() }

    interface Callback {
        fun onCompletedData(completedChallenge: CompletedData)
        fun onAuthoredData(authoredChallenge: AuthoredData)
        fun onError(message: String)
        fun onLoading(boolean: Boolean)
    }

    var callback: Callback? = null

    fun attach(callback: Callback, type: Int, challengeId: String) {
        this.callback = callback

        when (type) {
            0 -> getCompletedData(challengeId)
            1 -> getAuthoredData(challengeId)
        }
    }

    fun detach() {
        this.callback = null
        disposables.clear()
    }

    private fun getCompletedData(challengeId: String) {
        searchService.getCompletedData(challengeId)
            .subscribeOn(Schedulers.io())
            .doOnSubscribe { callback?.onLoading(true) }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                callback?.onCompletedData(it)
                callback?.onLoading(false)
            }, {
                callback?.onLoading(false)
                it.message?.apply {
                    callback?.onError(this)
                }
            }).also {
                disposables.add(it)
            }
    }

    private fun getAuthoredData(challengeId: String) {
        searchService.getAuthoredData(challengeId)
            .subscribeOn(Schedulers.io())
            .doOnSubscribe { callback?.onLoading(true) }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                callback?.onLoading(false)
                callback?.onAuthoredData(it)
            }, {
                callback?.onLoading(false)
                it.message?.apply {
                    callback?.onError(this)
                }
            }).also {
                disposables.add(it)
            }
    }
}