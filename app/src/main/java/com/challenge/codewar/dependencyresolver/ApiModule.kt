package com.challenge.codewar.dependencyresolver

import com.challenge.codewar.network.CodewarsService
import com.challenge.codewar.network.RestClientFactory
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class ApiModule {

    @Provides
    internal fun provideCodewarsService(client: OkHttpClient): CodewarsService {
        return RestClientFactory.createRetrofitRestClient("https://www.codewars.com/api/v1/", client)
    }

    @Provides
    @Singleton
    internal fun provideOkHttpClient(): OkHttpClient {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        return OkHttpClient().newBuilder().readTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS)
            .connectTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS)
            .writeTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS).addInterceptor(interceptor).build()
    }

    companion object {
        const val CONNECTION_TIMEOUT: Long = 40
    }
}
