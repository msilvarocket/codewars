package com.challenge.codewar.dependencyresolver

import com.challenge.codewar.CodeWarsApplication
import com.challenge.codewar.challengedetailsscreen.ChallengeDetailsFragment
import com.challenge.codewar.mainscreen.MainActivity
import com.challenge.codewar.memberscreen.MemberDetailsFragment
import com.challenge.codewar.searchscreen.SearchFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [
    ApplicationModule::class,
    ApiModule::class,
    PresentersModule::class,
    ServicesModule::class,
    RepositoriesModule::class
])
interface ApplicationComponent {

    // Application
    fun inject(app: CodeWarsApplication)

    // Activities
    fun inject(activity: MainActivity)

    // Fragments
    fun inject(fragment: SearchFragment)
    fun inject(fragment: MemberDetailsFragment)
    fun inject(fragment: ChallengeDetailsFragment)
}