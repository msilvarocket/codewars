package com.challenge.codewar.dependencyresolver

import com.challenge.codewar.database.MemberRoomDatabase
import com.challenge.codewar.shared.MemberRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepositoriesModule {

    @Singleton
    @Provides
    fun provideMemberRepository(db : MemberRoomDatabase): MemberRepository {
        return MemberRepository(db)
    }

}
