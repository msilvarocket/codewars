package com.challenge.codewar.dependencyresolver

import com.challenge.codewar.challengedetailsscreen.ChallengeDetailsPresenter
import com.challenge.codewar.memberscreen.MemberDetailsPresenter
import com.challenge.codewar.searchscreen.SearchPresenter
import com.challenge.codewar.searchscreen.SearchService
import com.challenge.codewar.shared.SchedulerProvider
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class PresentersModule {

    @Singleton
    @Provides
    fun provideSearchPresenter(searchService: SearchService, schedulerProvider: SchedulerProvider): SearchPresenter {
        return SearchPresenter(searchService, schedulerProvider)
    }

    @Singleton
    @Provides
    fun provideMemberDetailsPresenter(searchService: SearchService): MemberDetailsPresenter {
        return MemberDetailsPresenter(searchService)
    }

    @Singleton
    @Provides
    fun provideChallengeDetailsPresenter(searchService: SearchService): ChallengeDetailsPresenter {
        return ChallengeDetailsPresenter(searchService)
    }

}
