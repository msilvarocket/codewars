package com.challenge.codewar.dependencyresolver

import android.app.Application
import android.content.Context
import androidx.room.Room
import com.challenge.codewar.database.MemberRoomDatabase
import com.challenge.codewar.shared.AppSchedulerProvider
import com.challenge.codewar.shared.SchedulerProvider
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ApplicationModule(private val mApp: Application) {

    @Provides
    fun provideContext(): Context = mApp

    @Singleton
    @Provides
    fun provideRoomDatabase(context: Context) : MemberRoomDatabase {
        return Room.databaseBuilder(
            context,
            MemberRoomDatabase::class.java, "members.db"
        ).build()
    }

    @Provides
    fun provideSchedulersProvider() : SchedulerProvider {
        return AppSchedulerProvider()
    }
}
