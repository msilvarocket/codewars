package com.challenge.codewar.dependencyresolver

import com.challenge.codewar.network.CodewarsService
import com.challenge.codewar.searchscreen.SearchService
import com.challenge.codewar.shared.MemberRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ServicesModule {

    @Singleton
    @Provides
    fun provideSearchService(codewarsService: CodewarsService, memberRepository: MemberRepository): SearchService {
        return SearchService(codewarsService, memberRepository)
    }

}
