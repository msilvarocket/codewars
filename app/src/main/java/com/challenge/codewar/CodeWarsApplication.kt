package com.challenge.codewar

import android.app.Application
import com.challenge.codewar.dependencyresolver.ApplicationComponent
import com.challenge.codewar.dependencyresolver.ApplicationModule
import com.challenge.codewar.dependencyresolver.DaggerApplicationComponent

class CodeWarsApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        applicationComponent = DaggerApplicationComponent.builder()
            .applicationModule(ApplicationModule(this))
            .apiModule(com.challenge.codewar.dependencyresolver.ApiModule())
            .presentersModule(com.challenge.codewar.dependencyresolver.PresentersModule())
            .servicesModule(com.challenge.codewar.dependencyresolver.ServicesModule())
            .repositoriesModule(com.challenge.codewar.dependencyresolver.RepositoriesModule())
            .build()
    }

    companion object {
        lateinit var applicationComponent: ApplicationComponent
    }
}