package com.challenge.codewar.searchscreen

import com.challenge.codewar.network.pojo.WrapperMember
import com.challenge.codewar.network.pojo.WrapperMember.Companion.STATUS_FOUND
import com.challenge.codewar.network.pojo.WrapperMember.Companion.STATUS_REMOVED
import com.challenge.codewar.shared.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

open class SearchPresenter @Inject constructor(private val searchService: SearchService, private val schedulerProvider: SchedulerProvider) {

    private val disposables by lazy { CompositeDisposable() }

    open interface Callback {
        open fun onCachedSearches(members: MutableList<WrapperMember.Add>)
        fun onEmptyCache()
        fun onSearchFound(member: WrapperMember.Add)
        fun onRemoveElement(member: WrapperMember.Delete)
        fun onSearchNotFound()
        open fun onSearchError(message: String)
        fun onLoading(boolean: Boolean)
    }

    var callback: Callback? = null

    fun attach(callback: Callback) {
        this.callback = callback

        prepareMemberUpdateObserver()
        getFoundCacheMembers()
    }

    /**
     * subscribe stream that will handle Member Search results
     */
    fun prepareMemberUpdateObserver() {
        searchService.getMemberStream().observeOn(schedulerProvider.ui())
            .subscribe {
                wrapperMemberChangeReceived(it)
            }.also {
                disposables.add(it)
            }
    }

    /**
     * Checks the type of WrapperMember to know whether should add/remove or if an error occured.
     */
    fun wrapperMemberChangeReceived(it: WrapperMember?) {
        when (it) {
            is WrapperMember.Add -> {
                when (it.status) {
                    STATUS_FOUND -> {
                        this.callback?.onSearchFound(it)
                    }

                }
            }
            is WrapperMember.Delete -> {
                when (it.status) {
                    STATUS_REMOVED -> {
                        this.callback?.onRemoveElement(it)
                    }
                }
            }
            is WrapperMember.Error -> {
                this.callback?.onSearchError(it.errorMessage)
            }
        }
        this.callback?.onLoading(false)
    }

    fun detach() {
        this.callback = null
        disposables.clear()
    }

    /**
     * Get Members successfully searched previously
     */
    open fun getFoundCacheMembers() {
        searchService.foundMembers()
            .subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.ui())
            .map { it.toMutableList() }
            .subscribe({ list->
                callback?. onCachedSearches(list)
            }, {
                callback?.onEmptyCache()
            }).also {
                disposables.add(it)
            }
    }

    fun searchMember(userName: String) {
        if (userName.length < 3) {
            callback?.onSearchError("Please provide a member with more then 3 characters.")
        } else {
            searchService.searchMember(userName)
            callback?.onLoading(true)
        }
    }
}