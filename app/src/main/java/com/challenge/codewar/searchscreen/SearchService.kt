package com.challenge.codewar.searchscreen

import android.util.Log
import com.challenge.codewar.database.pojo.*
import com.challenge.codewar.network.CodewarsService
import com.challenge.codewar.network.pojo.WrapperMember
import com.challenge.codewar.network.pojo.WrapperMember.Companion.STATUS_FAILED
import com.challenge.codewar.network.pojo.WrapperMember.Companion.STATUS_FOUND
import com.challenge.codewar.network.pojo.WrapperMember.Companion.STATUS_REMOVED
import com.challenge.codewar.shared.MemberRepository
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject

open class SearchService(private val codewarsService: CodewarsService, private val memberRepository: MemberRepository) {
    private val processor = PublishSubject.create<WrapperMember>()
    private val streamMember: Observable<WrapperMember> = processor.distinct()

    open fun getMemberStream(): Observable<WrapperMember> {
        return streamMember
    }

    fun searchMember(userName: String) {
        codewarsService.searchUser(userName)
            .subscribeOn(Schedulers.io())
            .map { response ->
                if (response.isSuccessful) {
                    response.body()?.let {
                        Pair(it, memberRepository.elementToRemove(it))
                    }

                } else {
                    Log.w("SearchService", response.message())
                    var message = response.message()
                    if ( response.code() == 404 ) {
                        message = "Search not found"
                    }
                    processor.onNext(WrapperMember.Error(STATUS_FAILED, message))
                    Pair(null, null)
                }
            }
            .subscribe(
                {
                    it?.second?.let { member ->
                        when (member) {
                            is WrapperMember.Delete -> {
                                memberRepository.removeFromCache(member.username)
                                processor.onNext(
                                    WrapperMember.Delete(
                                        STATUS_REMOVED,
                                        member.username
                                    )
                                )
                            }
                        }
                    }

                    it?.first?.let { memberResponse ->
                        val memberComplete = memberRepository.addToCache(memberResponse)
                        memberComplete.apply {
                            processor.onNext(
                                WrapperMember.Add(
                                    STATUS_FOUND,
                                    this
                                )
                            )
                        }
                    }

                }, {
                    Log.w("SearchService", it)
                    it.message?.let { message ->
                        processor.onNext(WrapperMember.Error(STATUS_FAILED, message)) // Error during request
                    }
                }).also { }
    }

    open fun foundMembers(): Single<List<WrapperMember.Add>> {
        return Single.fromCallable {
            memberRepository.foundMembers().map { member ->
                Log.i("code1", "date ${member.date}")
                WrapperMember.Add(
                    STATUS_FOUND,
                    member
                )
            }
        }
    }

    fun getAuthored(userName: String): Single<AuthoredChallenges> {
        return Single.fromCallable {
            codewarsService.getAuthored(userName)
                .map {
                    if (it.isSuccessful) {
                        it.body()?.let { body ->
                            memberRepository.addAuthoredChallenges(userName, body)
                        }
                    } else {
                        AuthoredChallenges(userName)
                    }
                }.onErrorReturn { memberRepository.getAuthoredChallenges(userName) }.subscribeOn(Schedulers.io()).blockingGet()
        }
    }

    fun getChallengesCompleted(userName: String, page: Int): Single<CompletedChallenges> {
        return Single.fromCallable {
            codewarsService.getChallengesCompleted(userName, page)
                .map {
                    if (it.isSuccessful) {
                        it.body()?.let { body ->
                            memberRepository.addCompletedChallengesPage(userName, body)
                        }
                    } else {
                        CompletedChallenges(userName, 0, 0)
                    }
                }.onErrorReturn {
                    memberRepository.getCompletedChallengesPage(userName) }.subscribeOn(Schedulers.io()).blockingGet()
        }
    }

    fun getNextChallengesCompleted(userName: String, page: Int): Single<CompletedChallenges> {
        return Single.fromCallable {
            codewarsService.getChallengesCompleted(userName, page)
                .map {
                    if (it.isSuccessful) {
                        it.body()?.let { body ->
                            memberRepository.addCompletedChallengesPage(userName, body)
                        }
                    } else {
                        CompletedChallenges(userName, 0, 0)
                    }
                }.subscribeOn(Schedulers.io()).blockingGet()
        }
    }

    fun getMemberComplete(userName: String): Single<MemberComplete> {
        return Single.fromCallable {
            memberRepository.getMemberComplete(userName)
        }
    }

    fun getCompletedData(id: String): Single<CompletedData> {
        return Single.fromCallable {
            memberRepository.getCompletedData(id)
        }
    }

    fun getAuthoredData(id: String): Single<AuthoredData> {
        return Single.fromCallable {
            memberRepository.getAuthoredData(id)
        }
    }
}