package com.challenge.codewar.searchscreen

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.challenge.codewar.R
import com.challenge.codewar.network.pojo.WrapperMember

class SearchAdapter(private val mutableList: MutableList<WrapperMember.Add>,
                    private val callback: Callback) : RecyclerView.Adapter<SearchAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).
                inflate(
                    R.layout.adapter_search_element, parent,
                    false))
    }

    override fun getItemCount(): Int {
        return mutableList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        mutableList[position].let { member ->
            member.memberComplete.apply {
                holder.name.text = username
                holder.rank.text = leaderboardPosition.toString()
                val language = languages?.maxBy{ l -> l.score!! }
                holder.language.text = "${language?.language} ${language?.score}"
                holder.itemView.setOnClickListener {
                    callback.onItemClicked(member)
                }
            }
        }
    }

    fun addMember(orderByRank: Boolean, member: WrapperMember.Add) {
        if(mutableList.none { it.memberComplete.username == member.memberComplete.username }){
            mutableList.add(0, member)
            notifyItemInserted(0)
            if(orderByRank) {
                mutableList.sortBy { member-> member.memberComplete.leaderboardPosition }
                notifyDataSetChanged()
            }
        } else {
            mutableList.removeAll{ it.memberComplete.username == member.memberComplete.username }
            mutableList.add(0, member)
            if(orderByRank) {
                mutableList.sortBy { member-> member.memberComplete.leaderboardPosition }
            }
            notifyDataSetChanged()
        }
    }

    fun removeMember(member: WrapperMember.Delete) {
        mutableList.removeAll{ it.memberComplete.username == member.username }
        notifyDataSetChanged()
    }

    fun addMembers(members: MutableList<WrapperMember.Add>){
        mutableList.clear()
        mutableList.addAll(members)
        notifyDataSetChanged()
    }

    fun reorder(byPosition: Boolean) {
        if(byPosition) {
            mutableList.sortBy { member-> member.memberComplete.leaderboardPosition }
        } else {
            mutableList.sortByDescending { member-> member.memberComplete.date }
        }
        notifyDataSetChanged()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val name : TextView = itemView.findViewById(R.id.name)
        val rank : TextView = itemView.findViewById(R.id.rank)
        val language : TextView = itemView.findViewById(R.id.language)
    }

    interface Callback {
        fun onItemClicked(member: WrapperMember.Add)
    }
}