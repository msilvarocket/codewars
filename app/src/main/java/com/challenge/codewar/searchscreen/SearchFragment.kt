package com.challenge.codewar.searchscreen

import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.recyclerview.widget.DividerItemDecoration
import com.challenge.codewar.CodeWarsApplication
import com.challenge.codewar.R
import com.challenge.codewar.extensions.hideKeyboard
import com.challenge.codewar.extensions.setOnThrottleClickListener
import com.challenge.codewar.mainscreen.MainActivity
import com.challenge.codewar.network.pojo.WrapperMember
import kotlinx.android.synthetic.main.fragment_search.*
import javax.inject.Inject

class SearchFragment : Fragment(), SearchPresenter.Callback, SearchAdapter.Callback {

    @Inject
    lateinit var presenter: SearchPresenter

    private val adapter: SearchAdapter = SearchAdapter(mutableListOf(), this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        CodeWarsApplication.applicationComponent.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_search, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.apply {
            title = getString(R.string.app_name)
            when(this) {
                is MainActivity -> showBackButton(false)
            }
        }
        searchButton.setOnThrottleClickListener {
            it.hideKeyboard()
            presenter.searchMember(editText.text.toString())
        }

        reorder.setOnThrottleClickListener {
            val isRank = reorder.text.equals(it.context.getString(R.string.by_rank))
            adapter.reorder(isRank)
            reorder.text = it.context.getString(if (isRank) R.string.by_date else R.string.by_rank)
        }

        presenter.attach(this)
        recyclerView?.let {
            it.addItemDecoration(DividerItemDecoration(it.context, DividerItemDecoration.VERTICAL))
            it.adapter = adapter
        }
        editText.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_ENTER && event.action == KeyEvent.ACTION_UP) {
                v.hideKeyboard()
                presenter.searchMember(editText.text.toString())
                return@OnKeyListener true
            }
            false
        })
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detach()
    }

    /**
     * Presenter Callbacks
     */
    override fun onSearchNotFound() {
        Toast.makeText(context, "onSearchNotFound", Toast.LENGTH_LONG).show()
    }

    /**
     * Founded a member, lets added to the list.
     * Check the order button text to know where to put it on the list.
     */
    override fun onSearchFound(member: WrapperMember.Add) {
        editText.text?.clear()
        adapter.addMember(reorder.text.equals(reorder.context.getString(R.string.by_date)), member)
        recyclerView?.visibility = VISIBLE
        emptyList?.visibility = GONE
    }

    override fun onRemoveElement(member: WrapperMember.Delete) {
        adapter.removeMember(member)
    }

    override fun onLoading(boolean: Boolean) {
        if (boolean) {
            loading.visibility = VISIBLE
        } else {
            loading.visibility = GONE
        }
    }

    override fun onCachedSearches(members: MutableList<WrapperMember.Add>) {
        adapter.addMembers(members)
        recyclerView?.visibility = VISIBLE
        emptyList?.visibility = GONE
    }

    override fun onEmptyCache() {
        recyclerView?.visibility = GONE
        emptyList?.visibility = VISIBLE
    }

    override fun onSearchError(message: String) {
        Toast.makeText(context, "$message", Toast.LENGTH_LONG).show()
    }

    /**
     * SearchAdapter.Callback for item click
     */
    override fun onItemClicked(member: WrapperMember.Add) {
        activity?.let {
            emptyList?.hideKeyboard()
            Navigation.findNavController(it, R.id.nav_fragment).navigate(R.id.member_details_fragment, Bundle().apply {
                putString("username", member.memberComplete.username)
            })
        }
    }
}