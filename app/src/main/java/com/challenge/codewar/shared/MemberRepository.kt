package com.challenge.codewar.shared

import com.challenge.codewar.database.MemberRoomDatabase
import com.challenge.codewar.database.pojo.*
import com.challenge.codewar.network.pojo.AuthoredChallengesResponse
import com.challenge.codewar.network.pojo.CompletedChallengesResponse
import com.challenge.codewar.network.pojo.MemberResponse
import com.challenge.codewar.network.pojo.WrapperMember
import com.challenge.codewar.network.pojo.WrapperMember.Companion.STATUS_FAILED
import com.challenge.codewar.network.pojo.WrapperMember.Companion.STATUS_TO_REMOVE
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

class MemberRepository constructor(val db: MemberRoomDatabase) {

    fun foundMembers(): MutableList<MemberComplete> {
        return Single.fromCallable {
            db.memberDao().getCachedMembers()
        }.subscribeOn(Schedulers.single()).blockingGet()
    }

    fun elementToRemove(memberResponse: MemberResponse): WrapperMember? {
        return Single.fromCallable {
            if (db.memberDao().containsMember(memberResponse.username) == 0 && db.memberDao().getNumberOfMembers() > 4) {
                db.memberDao().getMemberToDelete().let { member ->
                    WrapperMember.Delete(STATUS_TO_REMOVE, member.username)
                }
            } else {
                WrapperMember.Error(STATUS_FAILED, "")
            }
        }.subscribeOn(Schedulers.single()).blockingGet()
    }

    fun addToCache(memberResponse: MemberResponse): MemberComplete {
        return Single.fromCallable {
            db.memberDao().inserMemberResponse(memberResponse)
        }.subscribeOn(Schedulers.single()).blockingGet()
    }

    fun getMemberComplete(username: String): MemberComplete {
        return Single.fromCallable {
            db.memberDao().getMemberComplete(username)
        }.subscribeOn(Schedulers.single()).blockingGet()
    }

    fun getCompletedData(id: String): CompletedData {
        return Single.fromCallable {
            db.memberDao().getCompletedData(id)
        }.subscribeOn(Schedulers.single()).blockingGet()
    }

    fun getAuthoredData(id: String): AuthoredData {
        return Single.fromCallable {
            db.memberDao().getAuthoredData(id)
        }.subscribeOn(Schedulers.single()).blockingGet()
    }

    fun removeFromCache(username: String) {
        Completable.fromCallable {
            db.memberDao().delete(username)
        }.subscribeOn(Schedulers.single()).blockingAwait()
    }

    fun addCompletedChallengesPage(
        username: String,
        completedChallengesResponse: CompletedChallengesResponse
    ): CompletedChallenges {
        return Single.fromCallable {
            db.memberDao().inserCompletedChallenges(username, completedChallengesResponse)
        }.subscribeOn(Schedulers.single()).blockingGet()
    }

    fun addAuthoredChallenges(
        username: String,
        authoredChallengesResponse: AuthoredChallengesResponse
    ): AuthoredChallenges {
        return Single.fromCallable {
            db.memberDao().inserAuthoredChallenges(username, authoredChallengesResponse)
        }.subscribeOn(Schedulers.single()).blockingGet()
    }

    fun getAuthoredChallenges(username: String): AuthoredChallenges {
        return Single.fromCallable {
            db.memberDao().getAuthoredChallenges(username)
        }.subscribeOn(Schedulers.single()).blockingGet()
    }

    fun getCompletedChallengesPage(username: String): CompletedChallenges {
        return Single.fromCallable {
            db.memberDao().getCompletedChallenges(username)
        }.subscribeOn(Schedulers.single()).blockingGet()
    }
}