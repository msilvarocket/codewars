package com.challenge.codewar

import com.challenge.codewar.database.pojo.MemberComplete
import com.challenge.codewar.network.pojo.WrapperMember
import com.challenge.codewar.network.pojo.WrapperMember.Companion.STATUS_FOUND
import com.challenge.codewar.searchscreen.SearchPresenter
import com.challenge.codewar.searchscreen.SearchService
import com.challenge.codewar.shared.TestSchedulerProvider
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.subjects.PublishSubject
import org.junit.After
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.mock


/**
 * SearchPresenter Unit tests
 */
class SearchPresenterUnitTest {
    val searchService = mock(SearchService::class.java)
    val testSchedulerProvider = TestSchedulerProvider()
    val presenter = SearchPresenter(searchService, testSchedulerProvider)
    private val processor = PublishSubject.create<WrapperMember>()
    val streamMember: Observable<WrapperMember> = processor.distinct()

    var callback: SearchPresenter.Callback = mock(SearchPresenter.Callback::class.java)
    val member = WrapperMember.Add(STATUS_FOUND, MemberComplete("test"))
    val listWrapperAdd: MutableList<WrapperMember.Add> = mutableListOf(member)

    /**
     * See [Memory leak in mockito-inline...](https://github.com/mockito/mockito/issues/1614)
     */
    @After
    fun clearMocks() {
        Mockito.framework().clearInlineMocks()
    }

    @Test
    fun attach_success() {
        // prepare
        Mockito.`when`(searchService.getMemberStream()).thenReturn(streamMember)
        Mockito.`when`(searchService.foundMembers()).thenReturn(Single.just(listWrapperAdd))

        // Act
        presenter.attach(callback)

        // Assert
        Mockito.verify(searchService, Mockito.times(1)).getMemberStream()
        Mockito.verify(searchService, Mockito.times(1)).foundMembers()
    }

    @Test
    fun foundMembers_success() {
        // Prepare
        attach_success()
        Mockito.`when`(searchService.foundMembers()).thenReturn(Single.just(listWrapperAdd))

        // Act
        presenter.getFoundCacheMembers()

        // Assert Called twice due to the first call on attach_success
        Mockito.verify(callback, Mockito.times(2)).onCachedSearches(listWrapperAdd)
    }

    @Test
    fun searchMembers_size_error() {
        // Prepare
        attach_success()

        // Act
        presenter.searchMember("te")

        // Assert
        Mockito.verify(callback, Mockito.times(1)).onSearchError("Please provide a member with more then 3 characters.")

    }
}
