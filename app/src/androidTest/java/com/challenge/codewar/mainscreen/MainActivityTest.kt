package com.challenge.codewar.mainscreen


import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.RootMatchers.withDecorView
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import com.challenge.codewar.R
import org.hamcrest.Matchers
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@LargeTest
@RunWith(AndroidJUnit4::class)
class MainActivityTest {

    @Rule
    @JvmField
    var mActivityTestRule = ActivityTestRule(MainActivity::class.java)

    @Test
    fun searchFragmentTest_showLoading() {
        onView(withId(R.id.editText)).check(matches(isDisplayed()))
        onView(withId(R.id.editText)).perform(typeText("sea"))
        onView(withId(R.id.searchButton)).perform(click())
        onView(withId(R.id.loading)).check(matches(isDisplayed()))
    }

    @Test
    fun searchFragmentTest_showErrorMessage() {
        onView(withId(R.id.editText)).check(matches(isDisplayed()))
        onView(withId(R.id.editText)).perform(typeText("En"))
        onView(withId(R.id.searchButton)).perform(click())
        onView(withText("Please provide a member with more then 3 characters.")).inRoot(withDecorView(
            Matchers.not(Matchers.`is`(mActivityTestRule.activity.window.decorView))))
            .check(matches(isDisplayed()))
    }
}
